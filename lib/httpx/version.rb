# frozen_string_literal: true

module HTTPX
  VERSION = "0.18.7"
end
